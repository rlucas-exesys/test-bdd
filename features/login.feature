Feature: Log in to web portal

	Insurers should be able to look log in to the web portal
	This enables them to submit underwriting and claims bordereaux
	and also to perform address searches.

	Rules:
	- User must have a valid client certificate
	- User must have a valid username and password
    - User must belong to an active company
    - User must be active
    - User must not have too many password attempts

    @nocert
	Scenario: Access denied without certificate
	    Given I have no certificate
        When I access the web portal
        Then I should be told that access is denied without a valid certificate

    Scenario: Login successfully
        Given I access the web portal with a valid certificate
        When I log in with my username and password
        Then I should be shown the welcome screen
        Then rob test 1
