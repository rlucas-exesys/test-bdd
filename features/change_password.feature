Feature: Change password

    Insurers should be able to set their own passwords one they have initially
    signed in. This enables them to set a secure password that they can remember
    or generate a longer password using a password management tool.

    Rules:
        - User must know the existing password
        - User must not have too many password attempts
        - The password must be at least 10 characters long
        - The password must be no more than 50 characters long
        - The password must be repeated successfully before submission

    Background:
        Given I have logged into the web portal
        And I am on the change password page

    Scenario: Incorrect current password
        When I enter an incorrect password
        And I submit the new password 'Password1!'
        Then I should be told that my username or password is incorrect

    Scenario: Account locked out after too many failed attempts
        When I submit an incorrect password too many times
        Then I should be told that my account is locked

    Scenario: Successfully change password
        When I submit my current password and a new password
        Then I should be told that my password was changed
        And I should be to log in with my new password

    Scenario: Password must be at least 10 characters
        Given I enter my password
        When I enter 'too short' as a new password
        Then I should be told that my password is too short
        And I should not be able to change my password

    Scenario: Show a warning when 50 characters reached
        Given I enter my password
        When I enter 'This password is exactly 50 characters long!!!!!!!' as a new password
        And I repeat my password is 'This password is exactly 50 characters long!!!!!!!'
        Then I should be warned that my password is at the maximum length
        But I should be able to change my password

    Scenario: Password must have a number
        Given I enter my password
        When I enter 'Password!!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to change my password

    Scenario: Password must have a lower case letter
        Given I enter my password
        When I enter 'PASSWORD1!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to change my password

    Scenario: Password must have an upper case letter
        Given I enter my password
        When I enter 'password1!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to change my password

    Scenario: Password must have a special character
        Given I enter my password
        When I enter 'Password12' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to change my password
