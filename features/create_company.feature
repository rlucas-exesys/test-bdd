Feature: Create Company

    Background:
        Given I am an admin on the create company page

    Scenario: No errors are displayed at first
        Then there should be no form errors shown

    Scenario: Name must be no more than 255 characters long
        When I enter valid details for a new company
        And I set the name to "This is a really long company name that is over two hundred and fifty five characters long and so it will fail the schema validation for the admin portal user management system and produce an error on the screen for the user to correct their submission...."
        And I submit the new company
        Then I should be told the name is too long

    Scenario: Name is required
        When I enter valid details for a new company
        And I set the name to ""
        And I submit the new company
        Then I should be told the name is required

    Scenario: Fr Insurers Id must match pattern
        When I enter valid details for a new company
        And I set the frInsurersId to "bang"
        And I submit the new company
        Then I should be told the frInsurersId is invalid