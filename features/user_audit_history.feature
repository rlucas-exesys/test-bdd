Feature: User Audit History

    Scenario: Audit history is in correct order
        Given I access the web portal with a valid certificate
        When I log in with my username and password
        And I access the web portal with a valid certificate
        And I log in with my username and password
        And I am an admin on the user audit history page
        Then there should be no form errors shown
        Then I should see my login history in the correct order