Feature: Single address search for the support team

    The support team should be able to perform single address searches so that
    they can assist insurers in submitting underwritings.  They should also be
    able to view the entire eligibility history so that they can investigate
    properties in more detail.

    Rules:
        - Prioritise key lookups (FRID, UPRN, PAF UDPRN)
        - When there is no matching key, attempt to match by address lines
        - Can see the full history of eligibility information

    Background:
        Given I have logged into the web portal
        And I am on the single address lookup page
        And the following are addresses that have a match:
        	| Line 1    | Postcode | Matching                             |
        	| 10        | SW1A 2AA | a3482087-65d8-47b1-9ecd-90e5fb0d5bad |

    Scenario: View eligibility history
        When I search for an address with a match
        And I view the eligibility history
        Then the history should be displayed

    Scenario: Shouldn't display popup if there are no results
        When I view the eligibility before searching
        Then the history should not be displayed
