Feature: Single address search

	Insurers should be able to look up the eligibility data and FRID for a single address.
	This enables them to determine the correct premium for an address as well as it's eligibility
	and also to be sure of identifying the correct address by it's unique id (FRID).

	Rules:
        - Prioritise key lookups (FRID, UPRN, PAF UDPRN)
        - When there is no matching key, attempt to match by address lines
        - Can only see the latest eligibility information

    Background:
        Given I do not have permission to view the eligibility history
        And I have logged into the web portal
        And I am on the single address lookup page
        And the following are keys that have a match:
            | Type      | Key                                  | Matching                             |
            | FRID      | fac6fe56-0133-4f36-945e-31754f2f815f | fac6fe56-0133-4f36-945e-31754f2f815f |
            | PAF UDPRN | 27999404                             | c79c0f24-1fe4-44ec-b4a2-3fd3efa90e8e |
            | UPRN      | 99999                                | fac6fe56-0133-4f36-945e-31754f2f815f |
        And the following are keys that do not have a match:
            | Type      | Key                                  |
            | FRID      | 45b93f5a-aa23-41f5-a36b-295ea0d18e85 |
            | PAF UDPRN | 52529836                             |
            | UPRN      | 99999999999                          |
        And the following are addresses that have a match:
            | Line 1    | Postcode | Matching                             |
            | 10        | SW1A 2AA | a3482087-65d8-47b1-9ecd-90e5fb0d5bad |

	Scenario: PAF UDPRN only without a match
	    When I search for a PAF UDPRN without a match
	    Then I should be shown a message stating there is no matching address

	Scenario: UPRN only without a match
	    When I search for a UPRN without a match
	    Then I should be shown a message stating there is no matching address

	Scenario: FRID only without a match
	    When I search for a FRID without a match
	    Then I should be shown a message stating there is no matching address

	Scenario: PAF UDPRN only with a match
	    When I search for a PAF UDPRN with a match
	    Then the results should show the FRID for that key

	Scenario: UPRN only with a match
	    When I search for a UPRN with a match
	    Then the results should show the FRID for that key

	Scenario: FRID only with a match
	    When I search for a FRID with a match
	    Then the results should show the FRID for that key

	Scenario: PAF UDPRN falls back to address if no match
	    When I search for a PAF UDPRN without a match but include a valid address
	    Then the results should show the FRID of the address

	Scenario: UPRN falls back to address if no match
	    When I search for a UPRN without a match but include a valid address
	    Then the results should show the FRID of the address

	Scenario: FRID falls back to address if no match
	    When I search for a FRID without a match but include a valid address
	    Then the results should show the FRID of the address

    Scenario: Address only
	    When I search for an address with a match
	    Then the results should show the FRID of the address

	Scenario: Address without a match
	    When I search for an address without a match
	    Then I should be shown a message stating there is no matching address

    Scenario: Search without any criteria
        When I search without entering any criteria
        Then I should be shown a message stating that my request was invalid

    Scenario: Clear search results
        When I search for an address with a match
        And I clear the form after the results are returned
        Then the results should be blank

    Scenario: Can't view the history without permission
        Given I do not have permission to view the eligibility history
        Then the history link should not be displayed
