Feature: Reset password

    Insurers should be able to reset their passwords if they forget them.
    This enables them to regain access to the web portal without needing to
    contact the service desk.

    Rules:
        - User must be able to access their registered email account
        - The password must be at least 10 characters long
        - The password must be no more than 50 characters long
        - The password must be repeated successfully before submission

    Background:
        Given I access the web portal with a valid certificate

    Scenario: Request reset from login page
        When I choose to reset my password from the login page
        Then I should receive an email with a reset password link

    Scenario: Reset password with valid token
        Given I have received a password reset link and followed it
        When I submit the reset password form with a valid password
        Then I should be told my password reset was successful
        And I should be able to log in with my reset password

    Scenario: Reset password with expired token
        Given I have received a password reset link and followed it
        But the password reset token has expired
        When I submit the reset password form with a valid password
        Then I should be told my token is invalid

    Scenario: Reset password with used token
        Given I have received a password reset link and followed it
        But the password reset token has been used already
        When I submit the reset password form with a valid password
        Then I should be told my token is invalid

    Scenario: Request a new token
        Given I have received a password reset link and followed it
        When I request a replacement reset token
        Then I should receive a new email with a reset password link

    Scenario: Password must be at least 10 characters
        Given I have received a password reset link and followed it
        When I enter 'too short' as a new password
        Then I should be told that my password is too short
        And I should not be able to reset my password

    Scenario: Show a warning when 50 characters reached
        Given I have received a password reset link and followed it
        When I enter 'This password is exactly 50 characters long!!!!!!!' as a new password
        And I repeat my password is 'This password is exactly 50 characters long!!!!!!!'
        Then I should be warned that my password is at the maximum length

    Scenario: Password must have a number
        Given I have received a password reset link and followed it
        When I enter 'Password!!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to reset my password

    Scenario: Password must have a lower case letter
        Given I have received a password reset link and followed it
        When I enter 'PASSWORD1!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to reset my password

    Scenario: Password must have an upper case letter
        Given I have received a password reset link and followed it
        When I enter 'password1!' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to reset my password

    Scenario: Password must have a special character
        Given I have received a password reset link and followed it
        When I enter 'Password12' as a new password
        Then I should be told that my password is not complex enough
        And I should not be able to reset my password
